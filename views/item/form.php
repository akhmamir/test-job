<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактирование товара';


?>

<div class="page-header">
    <h3 class="page-title"> Товары </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/item/">Товары</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактирование</li>
        </ol>
    </nav>
</div>

<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Редактирование товара</h4>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="form-group">
                <div class="col-12">
                        <?= $form->field($model, 'name')->textInput() ?>
                        <?= $form->field($model, 'price')->textInput() ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-12">
                    <?= $form->field($model, 'description')->textarea() ?>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                <a href="/crm/products/" class="btn btn-default" role="button">К списку продуктов</a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>