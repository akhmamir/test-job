<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Просмотр товаров';


?>

<div class="page-header">
    <h2 class="display1"> Товары </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/item/">Товары</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр</li>
        </ol>
    </nav>
</div>

<?php if (Yii::$app->session->hasFlash('success')) : ?>

    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="col-12 row">
    <div class="col-3">
        <p>
            <?= Html::a('Создать новый товар', ['add'], ['class' => 'btn btn-dark btn-fw']) ?>
        </p>
    </div>
    <div class="col-6">

    </div>
    <div class="col-3">
        <div class="form-group">
            <form class="search-form d-none d-md-block" action="/item/" method="get">
                <div class="input-group">
                    <input type="hidden" name="search" value="1">
                    <input type="text" class="form-control" placeholder="Название, описание и т.п." title="Поиск" name="search-string">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary" type="submit">Поиск</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <?php if(isset($_GET['search'])) : ?>
            <h2 class="display2"> Поиск по запросу: <?= $_GET['search-string'] ?> </h3>
            <?php endif ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [

                    [
                        'attribute' => 'name',
                        'format' => 'html',
                        'header' => 'Продукт',
                        'value' => function ($data) {
                            return $data['name'];
                        }
                    ],

                    [
                        'attribute' => 'description',
                        'format' => 'html',
                        'header' => 'Описание',
                        'value' => function ($data) {
                            return $data['description'];
                        }
                    ],

                    [
                        'attribute' => 'price',
                        'header' => 'Цена',
                        'value' => function ($data) {
                            return $data['price'] . ' ₽';
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'edit' => function ($url, $model) {
                                if (isset($_GET['page'])) {
                                    $page = $_GET['page'];
                                } else {
                                    $page = 1;
                                }
                                $customUrl = Yii::$app->getUrlManager()->createUrl(['/item/edit', 'id' => $model['id']]);
                                return Html::a('<span><i class="fas fa-pencil-alt" aria-hidden="true"></i> Редактировать</span>', $customUrl, [
                                    'class' => 'btn btn-inverse-primary btn-fw',
                                    'role' => 'button',
                                    'title' => 'Редактировать'
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                $customUrl = Yii::$app->getUrlManager()->createUrl(['/item/delete', 'id' => $model['id']]);
                                return Html::a('<span><i class="fa fa-ban" aria-hidden="true"></i> Удалить</span>', $customUrl, [
                                    'class' => 'btn btn-inverse-danger btn-fw',
                                    'role' => 'button',
                                    'title' => 'Удалить'
                                ]);
                            }
                        ],
                        'template' => '{edit} {delete}',
                    ],
                ],
                'tableOptions' => ['class' => 'table table-hover']
            ]); ?>
        </div>
    </div>
</div>