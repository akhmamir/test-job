<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Просмотр заказов';

$this->registerCssFile('/css/datepicker.min.css');
?>

<div class="page-header">
    <h2 class="display1"> Заказы </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/item/">Заказы</a></li>
                <li class="breadcrumb-item active" aria-current="page">Просмотр</li>
            </ol>
        </nav>
</div>

<?php if (Yii::$app->session->hasFlash('success')) : ?>

    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="col-12 row">
    <div class="col-3">
        <p>
            <?= Html::a('Создать новый заказ', ['add'], ['class' => 'btn btn-dark btn-fw']) ?>
        </p>
    </div>
    <div class="col-3">

    </div>
    <div class="col-6">
        <div class="form-group">
            <form class="search-form d-none d-md-block" action="/orders/" method="get">
                <div class="input-group">
                    <input id="date-picker" type="text" data-range="true" data-multiple-dates-separator=" - " class="form-control datepicker-here" name="date" />
                    <select class="form-control" name="status">
                        <option value="0">Любой</option>
                        <?php foreach ($statuses as $status) : ?>
                            <option value="<?= $status['id'] ?>"><?= $status['name'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <input type="hidden" name="search" value="1">
                    <input type="text" class="form-control" placeholder="Название, описание и т.п." title="Поиск" name="search-string">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary" type="submit">Поиск</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <?php if (isset($_GET['search'])) : ?>
                <h2 class="display2"> Поиск по запросу: <?= $_GET['search-string'] ?> </h3>
                <?php endif ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        [
                            'attribute' => 'number',
                            'format' => 'html',
                            'header' => 'Номер заказа',
                            'value' => function ($data) {
                                // print_r($data);
                                // die;
                                return $data['number'];
                            }
                        ],

                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'header' => 'Статус заказа',
                            'value' => function ($data) {
                                
                                return $data['status']['name'];
                            }
                        ],

                        [
                            'attribute' => 'price',
                            'header' => 'Цена',
                            'value' => function ($data) {
                                $summ = 0;
                                foreach ($data['items'] as $item) {
                                    $summ += $item['item']['price']*$item['count'];
                                }
                                return $summ . ' ₽';
                            }
                        ],

                        [
                            'attribute' => 'date',
                            'header' => 'Дата создания',
                            'value' => function ($data) {
                                return date('d.m.Y H:i', $data['date']);
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [
                                'edit' => function ($url, $model) {
                                    if (isset($_GET['page'])) {
                                        $page = $_GET['page'];
                                    } else {
                                        $page = 1;
                                    }
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['/orders/edit', 'id' => $model['id']]);
                                    return Html::a('<span><i class="fas fa-pencil-alt" aria-hidden="true"></i> Редактировать</span>', $customUrl, [
                                        'class' => 'btn btn-inverse-primary btn-fw',
                                        'role' => 'button',
                                        'title' => 'Редактировать'
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['/orders/delete', 'id' => $model['id']]);
                                    return Html::a('<span><i class="fa fa-ban" aria-hidden="true"></i> Удалить</span>', $customUrl, [
                                        'class' => 'btn btn-inverse-danger btn-fw',
                                        'role' => 'button',
                                        'title' => 'Удалить'
                                    ]);
                                }
                            ],
                            'template' => '{edit} {delete}',
                        ],
                    ],
                    'tableOptions' => ['class' => 'table table-hover']
                ]); ?>
        </div>
    </div>
</div>