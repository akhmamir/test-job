<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Редактирование заказа';

$Items = ArrayHelper::map($items, 'id', 'name');

$Statuses = ArrayHelper::map($orderStatus, 'id', 'name');
$StautusParams = [
    'class' => 'selectpicker',
    'data-width' => '100%',
    'title' => 'Выберите статус',
    'options' => [$model->id_status => ['Selected' => true]],
];


?>

<div class="page-header">
    <h3 class="page-title"> Заказы </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/item/">Заказы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактирование</li>
        </ol>
    </nav>
</div>

<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Редактирование заказа</h4>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="form-group">
                <div class="col-12">
                    <?= $form->field($model, 'number')->textInput() ?>
                    <?= $form->field($model, 'id_status')->dropDownList($Statuses, $StautusParams) ?>
                </div>
                <div class="col-12 row">
                    <div class="col-6">
                        <h5 class="card-title">Товар</h4>
                    </div>
                    <div class="col-6">
                        <h5 class="card-title">Количество</h4>
                    </div>
                </div>
                <div class="items">
                    <?php foreach ($orderItems as $ordIt) : ?>
                        <div class="exist-item row" id="order-item-block">
                            <div class="col-6" id="order-item">
                                <select id="orderitems-id_item" class="form-control form-control-sm" name="OrderItems[id_item]" title="Выберите товар" data-live-search="true" mobile="true" data-width="100%">
                                    <option value=""> </option>
                                    <?php foreach ($Items as $item => $name) : ?>
                                        <option value="<?= $item ?>" <?= ($ordIt['id_item'] == $item) ? ' selected' : '' ?>><?= $name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="col-6" id="order-item-count">
                                <input id="item-count" class="form-control form-control-sm count" type="text" value="<?= $ordIt['count'] ?>">
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="new-item row col-12">
                <div class="col-3">
                    <button type="button" class="btn btn-info btn-appent-item">Добавить</button>
                </div>
            </div>

            </br>

            <div class="form-group">
                <button type="button" class="btn btn-success btn-save-order">Сохранить</button>
                <a href="/orders/" class="btn btn-default" role="button">К списку заказов</a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    $('body')
        .on('click', '.btn-appent-item', function() {
            $('.items').append('<div class="exist-item row"><div class="col-6"><select id="orderitems-id_item" class="form-control form-control-sm" name="OrderItems[id_item]" title="Выберите товар" data-live-search="true" mobile="true" data-width="100%"><option value=""> </option><option value=""></option></select></div><div class="col-6"><input class="form-control form-control-sm count" type="text"></div></div>');
        })
        .on('click', '.btn-save-order', function() {
            var orderNumber = $('#orders-number').val();
            var orderStatus = $('#orders-id_status option:selected').val();

            var orderItems = $('.items');

            var itemsAndCounts = [];

            $('.items').each(function(index, value) {
                console.log(value);
            });
            $('*[id=order-item-block]:visible').each(function() {
                $(this).find('#orderitems-id_item option:selected').val()
                
                if (typeof itemsAndCounts[$(this).find('#orderitems-id_item option:selected').val()] !== "undefined") {
                    itemsAndCounts[$(this).find('#orderitems-id_item option:selected').val()] += Number($(this).find('#item-count').val());
                }
                else {
                    itemsAndCounts[$(this).find('#orderitems-id_item option:selected').val()] = Number($(this).find('#item-count').val())
                }
            });

            console.log(itemsAndCounts);

            $.ajax({
                        url: 'add',
                        data: {
                            orderNumber: orderNumber,
                            orderStatus: orderStatus,
                            items: itemsAndCounts
                            
                        },
                        dataType: 'json',
                        type: 'POST',
                        success: function (response) {
                            if (response.error === 0) {
                                console.log(response.data);
                                return document.location.href = 'http://34.94.53.225/orders/'
                            } else {
                                console.log(response.data);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr.responseText);
                        }
                    })
        })
</script>