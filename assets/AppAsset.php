<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendors/simple-line-icons/css/simple-line-icons.css',
        'vendors/flag-icon-css/css/flag-icon.min.css',
        'vendors/css/vendor.bundle.base.css',
        './vendors/daterangepicker/daterangepicker.css',
        './vendors/chartist/chartist.min.css',
        './css/style.css',
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
        'vendors/js/vendor.bundle.base.js',
        './vendors/chart.js/Chart.min.js',
        './vendors/moment/moment.min.js',
        './vendors/daterangepicker/daterangepicker.js',
        './vendors/chartist/chartist.min.js',
        'js/off-canvas.js',
        'js/misc.js',
        './js/dashboard.js',
        '/js/datepicker.js'
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
