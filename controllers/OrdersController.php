<?php


namespace app\controllers;

use app\models\Item;
use app\models\OrderItems;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Orders;
use app\models\OrderStatuses;
use yii\helpers\Json;

class OrdersController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
    /**
     * actionIndex
     * Orders Page
     * @return string
     */
    public function actionIndex()
    {

        $request = \Yii::$app->request;
        $statuses = OrderStatuses::find()->asArray()->all();

        if ($request->get('search')) {
            $searchData = Orders::find()->select('*')->with('items.item', 'status')->orderBy(['date' => SORT_DESC]);

            if ($request->get('status') > 0) {
                $searchData->where(['id_status' => $request->get('status')]);
            } else {
                $searchData->where(['not', ['id_status' => 0]]);
            }

            if ($request->get('search-string')) {
                $searchData->andWhere(['number' => $request->get('search-string')]);
            }

            if ($request->get('date')) {
                $DATE = explode('-', $request->get('date'));
                if (count($DATE) === 1) {
                    $STARTDATE = "-1 month";
                    $ENDDATE = $request->get('date');
                }
                if (count($DATE) === 2) {
                    $STARTDATE = $DATE[0];
                    $ENDDATE = $DATE[1];
                }
                $STARTDATE = strtotime($STARTDATE);
                $ENDDATE = strtotime($ENDDATE);

                $searchData->andWhere(['>=', 'date', $STARTDATE])->andWhere(['<=', 'date', $ENDDATE]);
            }


            $searchData->asArray();
            $dataProvider = new ActiveDataProvider([
                'query' => $searchData,
                'pagination' => [
                    'pageSize' => 30,
                ],
                'sort' => [
                    'attributes' => ['id'],
                ],
            ]);
        } else {

            $searchData = Orders::find()->with('items.item', 'status')->orderBy(['date' => SORT_DESC])->asArray()->all();
            $dataProvider = new ArrayDataProvider([
                'allModels' => $searchData,
                'pagination' => [
                    'pageSize' => 30,
                ],
                'sort' => [
                    'attributes' => ['id'],
                ],
            ]);
        }


        $dataProvider->pagination->pageSize = 30;

        return $this->render('index', compact('dataProvider', 'statuses'));
    }

    /**
     * actionAdd
     * to add Orders
     * @return string 
     */
    public function actionAdd()
    {

        $model = new Orders();

        $orderItems = new OrderItems();
        $items = Item::find()->all();
        $orderStatus = OrderStatuses::find()->all();
        if (\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            $itemsAjax = array_filter($post['items']);

            $model->id_status = $post['orderStatus'];
            $model->number = $post['orderNumber'];
            $model->date = strtotime('now');

            if ($model->save()) {
                foreach ($itemsAjax as $item => $count) {
                    $newOrderItem = new OrderItems();
                    $newOrderItem->id_order = $model->id;
                    $newOrderItem->id_item = $item;
                    $newOrderItem->count = (int)$count;
                    $newOrderItem->save();
                }

                \Yii::$app->session->setFlash('success', "Товар добавлен");
                return Json::encode([
                    'error' => 0,
                ]);
            }
        }


        return $this->render('form', compact('model', 'items', 'orderItems', 'orderStatus'));
    }

    /**
     * actionEdit
     * to edit orders
     * @param  int $id
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $orderItems = OrderItems::find()->where(['id_order' => $id])->with('item')->all();
        $items = Item::find()->all();
        $orderStatus = OrderStatuses::find()->all();

       if (\Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();
            $itemsAjax = array_filter($post['items']);

            $model->id_status = $post['orderStatus'];
            $model->number = $post['orderNumber'];
            $model->date = strtotime('now');

            if ($model->save()) {
                foreach ($itemsAjax as $item => $count) {
                    $newOrderItem = new OrderItems();
                    $newOrderItem->id_order = $model->id;
                    $newOrderItem->id_item = $item;
                    $newOrderItem->count = (int)$count;
                    $newOrderItem->save();
                }

                \Yii::$app->session->setFlash('success', "Товар добавлен");
                return Json::encode([
                    'error' => 0,
                ]);
            }
        }

        return $this->render('form', compact('model', 'items', 'orderItems', 'orderStatus'));
    }

    /**
     * actionDelete
     * to delete orders
     * @param  int $id
     *
     * @return void
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->session->setFlash('success', "Товар удалён");

        return $this->redirect(['index']);
    }

    /**
     * findModel
     * 
     * @param int $id
     *
     * @return \yii\db\ActiveQuery|yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
