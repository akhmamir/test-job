<?php


namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Item;

class ItemController extends Controller
{
    /**
     * actionIndex
     *
     * @return void
     */
    public function actionIndex()
    {

        $request = \Yii::$app->request;

        if ($request->get('search')) {
            $searchData = Item::findFromString($request->get('search-string'));
        }
        else {
            $searchData = Item::find()->orderBy(['name' => SORT_ASC])->asArray()->all();
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $searchData,
            'pagination' => [
                'pageSize' => 30,
            ],
            'sort' => [
                'attributes' => ['id'],
            ],
        ]);
        $dataProvider->pagination->pageSize = 30;

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * actionAdd
     * to add items
     *
     * @return string
     */
    public function actionAdd()
    {
        $model = new Item();

        if ($model->load(\Yii::$app->request->post()))
        {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', "Товар добавлен");
                return $this->redirect(["index"]);
            }
        }

        return $this->render('form', compact('model'));
    }

    /**
     * actionEdit
     * to edit items
     * @param  int $id
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()))
        {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', "Товар добавлен");
                return $this->redirect(["index"]);
            }
                
        }

        return $this->render('form', compact('model'));
    }

    /**
     * actionDelete
     * to delete items
     * @param  mixed $id
     *
     * @return void
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->session->setFlash('success', "Товар удалён");

        return $this->redirect(['index']);
    }

    /**
     * findModel
     * 
     * @param  mixed $id
     *
     * @return \yii\db\ActiveQuery|yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}