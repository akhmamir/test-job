<?php

namespace app\models;

use app\controllers\AppController;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $id_status
 * @property integer $date
 * @property string $number
 *
 * @property OrderItems[] $orderItems
 * @property OrderStatuses $idStatus
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_status', 'date'], 'integer'],
            [['number'], 'string', 'max' => 255],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatuses::className(), 'targetAttribute' => ['id_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_status' => 'Статус',
            'number' => 'Номер',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(OrderItems::className(), ['id_order' => 'id']);
    }

    public function getItem()
    {
        return $this->hasMany(Item::className(), ['id' => 'id_item'])
            ->via('items');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatuses::className(), ['id' => 'id_status']);
    }
}
