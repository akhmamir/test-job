<?php

namespace app\models;

use Yii;
/**
 * This is the model class for table "Item".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $price
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name', 'price'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasMany(OrderItems::className(), ['id_item' => 'id']);
    }
    
    public static function findFromString($string) {
        return \Yii::$app->db->createCommand(
            "SELECT * FROM item WHERE name LIKE :string OR description LIKE :string OR price LIKE :string",
            [':string' => "%$string%"]
        )->queryAll();
    }
}
